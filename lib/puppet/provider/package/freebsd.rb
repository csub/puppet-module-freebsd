require 'puppet/provider/package'
require 'net/ftp'

begin
  require 'puppet_x/csub/freebsd'
rescue LoadError
  require 'pathname'
  module_lib = Pathname.new(__FILE__).parent.parent.parent.parent
  require File.join module_lib, 'puppet_x/csub/freebsd'
end

Puppet::Type.type(:package).provide :freebsd, :parent => Puppet::Provider::Package do
  include Puppet::Util::Execution

  desc "The specific form of package management on FreeBSD. Resource names must be
  specified as the port origin: <port_category>/<port_name>."

  commands :pkginfo    => "/usr/sbin/pkg_info",
           :pkgadd     => "/usr/sbin/pkg_add",
           :pkgdelete  => "/usr/sbin/pkg_delete"

  confine :operatingsystem => :freebsd
  confine :feature => :bzip2
  defaultfor :operatingsystem => :freebsd
  has_feature :installable, :uninstallable, :upgradeable

  def self.instances
    packages = []
    output = pkginfo "-aoQ"
    output.split("\n").each do |data|
      pkg_string, pkg_origin = data.split(":")
      pkg_info = FreeBSD.parse_pkg_string(pkg_string)

      packages << new({
        :provider => self.name,
        :name     => pkg_origin,
        :ensure   => pkg_info[:pkg_version],
      })
    end
    packages
  end

  def source
    if @resource[:source]
      uri = URI.parse(@resource[:source])
      if uri.path.empty?
        uri.merge! FreeBSD.build_uri_path
      end
    else # source parameter not set; build default source URI
      uri = URI::HTTP.build({
        :host => "ftp.freebsd.org",
        :path => FreeBSD.build_uri_path,
      })
    end
    Puppet.debug "Package: #{@resource[:name]}: source => #{uri.inspect}"
    uri
  end

  def origin
    value = FreeBSD.parse_origin(@resource[:name])
    Puppet.debug "Package: #{@resource[:name]}: origin => #{value.inspect}"
    value
  end

  def index
    PortsIndexer.instance.get source
  end

  def package_uri
    begin
      pkg_info = index.fetch origin
    rescue IndexError
      raise Puppet::Error.new "package not found in INDEX"
    end
    pkg_name = FreeBSD.unparse_pkg_info pkg_info
    uri = source.merge File.join("All", pkg_name + ".tbz")
    Puppet.debug "Package: #{@resource[:name]}: package_uri => #{uri.inspect}"
    uri
  end

  def install
    # Source URI is for local file path.
    if !source.absolute? or source.scheme == "file"
      pkgadd source.path
    # Source URI is to specific package file
    elsif source.absolute? && source.path.end_with?(".tbz")
      pkgadd source.to_s
    # Source URI is to a package repository
    else
      pkgadd "-f", package_uri.to_s
    end
    nil
  end

  def update
    uninstall
    install
  end

  def latest
    pkg_info = index.fetch origin
    pkg_info[:pkg_version]
  end

  def query
    self.class.instances.each do |provider|
      if provider.name == @resource.name
        return provider.properties
      end
    end
    nil
  end

  def uninstall
    output = pkginfo "-qO", @resource[:name]
    output.split("\n").each { |pkg_name| pkgdelete('-f', pkg_name) }
  end
end
