require 'open-uri'
require 'net/ftp'
require 'singleton'

require 'bzip2' if Puppet.features.bzip2?

# fix bug in URI::FTP merge method that tries to set typecode
# even when other is a string.
class URI::FTP
  def merge(other)
    tmp = super(other)
    if self != tmp
      tmp.set_typecode(other.typecode) rescue NoMethodError
    end
    return tmp
  end
end

module FreeBSD
  def self.parse_pkg_string(pkg_string)
    {
      :pkg_name => pkg_string.split("-").slice(0..-2).join("-"),
      :pkg_version => pkg_string.split("-")[-1],
    }
  end

  def self.unparse_pkg_info(pkg_info)
    [:pkg_name, :pkg_version].map { |key| pkg_info[key] }.join("-")
  end

  def self.parse_origin(origin_path)
    begin
      origin = {
        :port_category => origin_path.split("/").fetch(-2),
        :port_name     => origin_path.split("/").fetch(-1),
      }
    rescue IndexError
      raise Puppet::Error.new "#{origin_path}: not in required origin format: .*/<port_category>/<port_name>"
    end
    origin
  end

  def self.build_uri_path
    Facter.loadfacts
    major_version = Facter.value(:kernelmajversion).split(".")[0]
    package_dir = ["packages", major_version, "stable"].join("-")
    File.join(
      "/", "pub", "FreeBSD", "ports",
      Facter.value(:hardwareisa),
      package_dir
    ) << "/"
  end
end

class PortsIndexer
  include Singleton

  def initialize
    @indexes = Hash.new
  end

  def get(source)
    @indexes[source] ||= build_index source
  end
  
  def build_index(source)
    index = Hash.new
    major_version = Facter.value(:kernelmajversion).split(".")[0]
    uri = source.merge "INDEX-#{major_version}.bz2"
    Puppet.debug "Fetching INDEX: #{uri.inspect}"
    begin
      Bzip2::Reader.open(uri) do |f|
        while (line = f.gets)
          fields = line.split("|")
          pkg_info = FreeBSD.parse_pkg_string(fields[0])
          origin = FreeBSD.parse_origin(fields[1])
          index[origin] = pkg_info
        end
      end
    rescue IOError, OpenURI::HTTPError, Net::FTPError
      raise Puppet::Error.new "Could not fetch ports INDEX: #{$!}"
    end
    index
  end
end
