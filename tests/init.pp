$package = [
  'shells/bash',
  'shells/zsh',
  'net/openldap24-sasl-client',
]
package { $package : ensure => latest }
